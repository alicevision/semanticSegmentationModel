# semanticSegmentationModel

ONNX model for Semantic Segmentation with [FcnResnet50](http://pytorch.org/vision/master/models/generated/torchvision.models.segmentation.fcn_resnet50.html).